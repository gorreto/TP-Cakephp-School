<?php

class Student extends AppModel {

    public $hasMany = 'Grade';

    public $validate = array(
        'first_name' => array(
            'rule'       => 'alphaNumeric',
            'required'   => true,
            'allowEmpty' => false,
            'message'    => 'Merci de renseigner le champ Prénom'
        ),
        'last_name' => array(
            'rule'       => 'alphaNumeric',
            'required'   => true,
            'allowEmpty' => false,
            'message'    => 'Merci de renseigner le champ Nom'
        ),
        'birthdate' => array(
            'is_date' => array(
                'rule'       => 'date',
                'required'   => true,
                'allowEmpty' => false,
                'message'    => 'Merci de renseigner le champ Nom'
            )
        )
    );

    public function afterFind($results, $primary = false) {
        return $results;
    }

    public function getAge(array $student) {
        $interval = date_diff(DateTime::createFromFormat('Y-m-d', $student['Student']['birthdate']), DateTime::createFromFormat('Y-m-d', date('Y-m-d')));
        return $interval->format('%y');
    }

    public function getFullName(array $student) {
        return $student['Student']['first_name'] . " " . $student['Student']['last_name'];
    }
}
