<?php

class Grade extends AppModel {

    public $belongsTo = 'Student';

    public $validate = array(
        'subject' => array(
            'rule'       => 'alphaNumeric',
            'required'   => true,
            'allowEmpty' => false,
            'message'    => 'Merci de renseigner le champ Matière'
        ),
        'number' => array(
            'rule'       => array('range', 0, 20),
            'required'   => true,
            'allowEmpty' => false,
            'message'    => 'Merci de renseigner le champ Note (de 0 à 20)'
        )
    );

    public function getAverage(array $grades) {
        $gradeNumbers = array_column($grades, 'number');

        return number_format(
            (
                array_sum($gradeNumbers) / count($gradeNumbers)
            ), 2, ',', ' '
        );
    }

}
