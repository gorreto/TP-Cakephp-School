<?php

class GradesController extends AppController {

    public function add($studentId){
        $this->set('fullname', $this->Grade->Student->getFullName($this->Grade->Student->findById($studentId)));

        if ($this->request->is('post')) {
            $this->Grade->create();
            $this->request->data['Grade']['student_id'] = $studentId;
            if ($this->Grade->save($this->request->data)) {
                $this->Session->setFlash(__('The Grade has been saved'));
                $this->redirect(array('controller' => 'students', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Grade could not e saved. Please, try again.'));
            }
        }
    }
}
