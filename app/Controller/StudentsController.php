<?php

class StudentsController extends AppController {

    public function index() {
        $this->Student->recursive = 1;

        $students = $this->Student->find('all', array(
                'fields'    => array('Student.last_name', 'Student.first_name', 'Student.birthdate'),
                'order'   => 'Student.last_name'
            )
        );

        foreach ($students as $key => $student) {
            if ( !empty($student['Grade'])) {
                $students[$key]['Grade']['average'] = $this->Student->Grade->getAverage($student['Grade']);
            }

            $students[$key]['Student']['birthdate'] = $this->Student->getAge($student);
        }

        $this->set('students', $students);
    }

    public function add(){
        if ($this->request->is('post')) {
            $this->Student->create();
            if ($this->Student->save($this->request->data)) {
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    public function edit($id = null){
        $student = $this->Student->findById($id);
        if (!$student) {
            throw new NotFoundException(__('Invalid student'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Student->id = $id;
            if ($this->Student->save($this->request->data)) {
                return $this->redirect(array('action' => 'index'));
            }
        }

        if (!$this->request->data) {
            $this->request->data = $student;
        }

        $this->set('student', $student);
    }

    public function delete($id = null, $cascade = true) {
        if ( $this->Student->delete($id)) {
            $this->redirect(array('action' => 'index'));
        }
    }
}
