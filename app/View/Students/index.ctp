<h1>Liste des résultats scolaire</h1>

<p>
    <?= $this->Html->link(
        'Ajouter un élève',
        array(
            'controller' => 'students',
            'action' => 'add'
        ),
        array(
            'class' => 'btn btn-outline-primary'
        )
    ); ?>
</p>

<table class="table table-striped table-bordered">
    <tr>
        <th class="text-center" scope="col">
            Nom
        </th>
        <th class="text-center" scope="col">
            Prénom
        </th>
        <th class="text-center" scope="col">
            Age
        </th>
        <th class="text-center" scope="col">
            Résultats
        </th>
        <th class="text-center" scope="col">
            Action
        </th>
    </tr>
    <?php foreach ($students as $key => $student): ?>
        <tr>
            <?php foreach ($student['Student'] as $key => $information): ?>
                <?php if ($key != 'id'): ?>
                    <td class="align-middle text-center">
                        <?= $information ?>
                    </td>
                <?php endif; ?>
            <?php endforeach; ?>
            <td class="align-middle text-center">
                <?php if(! empty($student['Grade'])): ?>
                    <table class="table table-bordered">
                        <tr>
                            <th scope="row">
                                Matière
                            </th>
                            <th scope="row">
                                Note
                            </th>
                        </tr>
                        <?php foreach ($student['Grade'] as $key => $information): ?>
                            <?php if (is_numeric($key)): ?>
                                <tr>
                                    <td class="align-middle text-center">
                                        <?= $information['subject'] ?>
                                    </td>
                                    <td class="align-middle text-center">
                                        <?= number_format($information['number'], 2, ',', ' '); ?> / 20
                                    </td>
                                </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <?php if (isset($student['Grade']['average'])): ?>
                            <tr class="table-info">
                                <td class="align-middle text-center">
                                    Moyenne
                                </td>
                                <td class="align-middle text-center">
                                    <?= $student['Grade']['average']; ?> / 20
                                </td>
                            </tr>
                        <?php endif; ?>
                    </table>
                <?php else: ?>
                    Aucune note renseignée
                <?php endif; ?>
            </td>
            <td class="align-middle text-center">
                <?= $this->Html->link(
                    'Éditer',
                    array(
                        'controller' => 'students',
                        'action' => 'edit',
                        $student['Student']['id']
                    ),
                    array(
                        'class' => 'btn btn-outline-primary'
                    )
                ); ?>
                <?= $this->Html->link(
                    'Supprimer',
                    array(
                        'controller' => 'students',
                        'action' => 'delete',
                        $student['Student']['id']
                    ),
                    array(
                        'confirm' => "Etes-vous sûr de bien supprimer la référence de l'élève ?",
                        'class' => 'btn btn-outline-danger'
                    )
                ); ?>
                <?= $this->Html->link(
                    'Ajouter une note',
                    array(
                        'controller' => 'grades',
                        'action' => 'add',
                        'id' => $student['Student']['id']
                    ),
                    array(
                        'class' => 'btn btn-outline-primary'
                    )
                ); ?>
            </td>
        </tr>
    <?php endforeach ?>
</table>


