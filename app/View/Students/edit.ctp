<h1>Éditer un élève</h1>

<?= $this->Form->create('Student'); ?>
    <?= $this->render('_form'); ?>
<?= $this->Form->end(array('label' => "Mettre à jour", 'class' => 'btn')); ?>
