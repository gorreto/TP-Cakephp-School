<?= $this->Form->input('first_name', array('label' => 'Votre prénom : ', 'placeholder' =>'Prénom', 'class' => 'form-control', 'div' => array( 'class' => 'form-group'))); ?>
<?= $this->Form->input('last_name', array('label' => 'Votre nom : ', 'placeholder' =>'Nom', 'class' => 'form-control', 'div' => array( 'class' => 'form-group'))); ?>
<?= $this->Form->input('birthdate', array('label' => 'Votre date de naissance : ', 'dateFormat' => 'DMY', 'minYear' => date('Y') - 30, 'maxYear' => date('Y') - 6)); ?>
