<h1>Renseigner une note à <?= $fullname; ?></h1>
<?= $this->Form->create('Grade'); ?>
    <?= $this->Form->input('subject', array('label' => 'Matière : ', 'placeholder' =>'Matière', 'class' => 'form-control', 'div' => array( 'class' => 'form-group'))); ?>
    <?= $this->Form->input('number', array('label' => 'Note : ', 'placeholder' =>'Note sur 20', 'class' => 'form-control', 'div' => array( 'class' => 'form-group'))); ?>
<?= $this->Form->end(array('label' => 'Enregistrer une note', 'class' => 'btn')); ?>
