<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><?= $this->fetch('title') ?></title>

    <!-- Bootstrap core CSS -->
    <?= $this->Html->css('bootstrap') ?>
    <?= $this->fetch('css') ?>
  </head>

  <body>

    <div class="container">

    	<?= $this->fetch('content'); ?>

    </div><!-- /.container -->


    <?= $this->Html->script('jquery-3.3.1.min.js') ?>
    <?= $this->Html->script('bootstrap') ?>
    <?= $this->fetch('script') ?>
  </body>
</html>
